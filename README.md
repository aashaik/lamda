# README #

A simple Twitter-like app built using Python - Flask, as part of the final project session  

See: http://lamda.herokuapp.com

### How do I get set up? ###

* Clone this repository on your local machine using the following command:
* "git clone https://arifali2016@bitbucket.org/arifali2016/lamda.git"
* Improve the app by adding new functionality
* Deploy it to your own heroku account.
* Configure the database
* and enjoy!

### Who do I talk to? ###

* arifali2016@gmail.com