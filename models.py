from main import db

class Squeak(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	poster = db.Column(db.Integer, db.ForeignKey('users.id'))
	text = db.Column(db.Text)
	favs = db.Column(db.Integer)
	timestamp = db.Column(db.Float)

	def __init__(self, _poster, _text, _favs, _timestamp):
		self.poster = _poster
		self.text = _text
		self.favs = _favs
		self.timestamp = _timestamp


class Users(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(100))
	email = db.Column(db.String(100))
	active = db.Column(db.Integer)
	password = db.Column(db.String(40))

	def __init__(self, _name, _email, _password, _active):
		self.name = _name
		self.email = _email
		self.active = _active
		self.password = _password


class Follower(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	who = db.Column(db.Integer, db.ForeignKey('users.id'))
	whom = db.Column(db.Integer, db.ForeignKey('users.id'))

	def __init__(self, _who, _whom):
		self.who = _who
		self.whom = _whom

